import argparse
import logging

from generator.generator import create_test


def main():
    logging.basicConfig()
    parser = argparse.ArgumentParser()
    parser.add_argument('questions_filename', type=str,
                        help='Specify filename with questions. File format is json')
    parser.add_argument('destination_folder', type=str,
                        help='Specify folder where save answers and test. File format is html')
    args = parser.parse_args()

    create_test(
        questions_filename=args.questions_filename,
        dst_folder=args.destination_folder,
    )
