import logging
import json
import random

from dataclasses import dataclass
from typing import List, Union

from jinja2 import Environment, PackageLoader
from marshmallow_dataclass import class_schema


logger = logging.getLogger(__name__)


@dataclass
class Question:
    question: str
    answers: List[str]
    right_answer: Union[int, List[int]]


AnswerSchema = class_schema(Question)


def load_env():
    return Environment(
        loader=PackageLoader('generator', 'resources'),
        autoescape=True,
    )


def load_questions(filename: str) -> List[Question]:
    schema = AnswerSchema(many=True)
    with open(filename) as f:
        questions = schema.load(json.load(f))

    # TODO: in case big array of questions will be slow. Need fix
    random.shuffle(questions)

    for question in questions:
        if isinstance(question.right_answer, int):
            question.right_answer = [question.right_answer]

        shuffle_question(question)
        yield question


def shuffle_question(question):
    question.answers = list(enumerate(question.answers, start=1))
    random.shuffle(question.answers)
    right_answer = []
    answers = []
    for new_index, answer_pair in enumerate(question.answers, start=1):
        old_index, answer = answer_pair
        if old_index in question.right_answer:
            right_answer.append(new_index)
        answers.append(answer)
    question.right_answer = right_answer
    question.answers = answers


def render_question(env: Environment, questions: List[Question]):
    answers_template = env.get_template('answers.html')
    test_template = env.get_template('test.html')

    return {
        'answers': answers_template.render(questions=questions),
        'test': test_template.render(questions=questions),
    }


def create_test(questions_filename: str, dst_folder: str):
    if questions_filename.rsplit('.', maxsplit=1)[-1] != 'json':
        logger.warning('File should be json')

    rendered = render_question(
        env=load_env(),
        questions=list(load_questions(questions_filename))
    )
    logger.info('templates has been rendered')
    for name, content in rendered.items():
        with open(f'{dst_folder}/{name}.html', 'w', encoding='utf-8') as f:
            f.write(content)
            logger.info(f'{name} has been written to disk in {dst_folder}')
