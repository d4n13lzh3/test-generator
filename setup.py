from pkg_resources import parse_requirements
from setuptools import find_packages, setup

module_name = 'generator'


def load_requirements(filename: str) -> list:
    with open(filename, 'r') as fp:
        for req in parse_requirements(fp.read()):
            extras = '[{}]'.format(','.join(req.extras)) if req.extras else ''
            yield '{}{}{}'.format(req.name, extras, req.specifier)


requirements = list(load_requirements('requirements.txt'))

setup(
    name=module_name,
    version='0.0.1',
    author='d4n13lzh3',
    author_email='zhe.dan28@gmail.com',
    platforms='all',
    classifiers=[
        'Intended Audience :: Developers',
        'Natural Language :: Russian',
        'Operating System :: MacOS',
        'Operating System :: POSIX',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.8',
        'Programming Language :: Python :: Implementation :: CPython'
    ],
    python_requires='>=3.7',
    setuo_requires=requirements,
    install_requires=requirements,
    packages=find_packages(),
    entry_points={
        'console_scripts': [
            '{0}-test = {0}.__main__:main'.format(module_name),
        ]
    },
    package_data={
        'generator': ['resources/*.html'],
    }
)
